#ifndef __QMC5883L__
#define __QMC5883L__

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif
#include <math.h>

#include <Array.h>
#include <Vector3f.h>
#include <Quaternion.h>

/*!
 * \class QMC5883L
 *
 * \brief A class dedicated to manage the QMC5883L magnetometer.
 *
 * This class is designed to manage a QMC5883L magnetometer device connected
 * to the Arduino using the I2C communication bus.
 * Its main goal is to compute the heading based on the magnetic earth field
 * and can be tilt compensated if both the pitch and roll angles are provided.
 * Thus, a filtering function can be activated to have smooth variations of the
 * final computed heading value.
 *
 * \author Charles Ingels
 *
 * \date 2020.02.21
 *
 */
class QMC5883L
{
	public:

		// The QMC5883L chip I2C address
		static const uint8_t QMC5883L_I2C_ADDR				= 0x0D;

		// QMC5883L registers definition
		static const uint8_t QMC5883L_OUTPUT_DATA_X_LSB_REG 	= 0;
		static const uint8_t QMC5883L_OUTPUT_DATA_X_MSB_REG 	= 0x1;
		static const uint8_t QMC5883L_OUTPUT_DATA_Y_LSB_REG	= 0x2;
		static const uint8_t QMC5883L_OUTPUT_DATA_Y_MSB_REG	= 0x3;
		static const uint8_t QMC5883L_OUTPUT_DATA_Z_LSB_REG	= 0x4;
		static const uint8_t QMC5883L_OUTPUT_DATA_Z_MSB_REG	= 0x5;
		static const uint8_t QMC5883L_STATUS_REG				= 0x6;
		static const uint8_t QMC5883L_TEMPERATURE_LSB_REG		= 0x7;
		static const uint8_t QMC5883L_TEMPERATURE_MSB_REG		= 0x8;
		static const uint8_t QMC5883L_CONTROL_1_REG			= 0x9;
		static const uint8_t QMC5883L_CONTROL_2_REG			= 0xA;
		static const uint8_t QMC5883L_PERIOD_REG				= 0xB;

		// The filtering depth. A high value gives better stability but higher
		// latency. A lower value gives less stability but more reactivity.
		// The value of 25 seems to be a quite good compromise but you can
		// experiment with other values.
		static const uint8_t QMC5883L_HISTORY_DATA			= 25;

		/*!
		 * \fn QMC5883L()
		 *
		 * \brief Constructor for the QMC5883L class.
		 *
		 * The class constructor. The "drdyPin" is the digital input pin
		 * on which the "Data ready" signal coming from the device is connected.
		 *
		 * \param
		 * drdyPin The input pin number of the Arduine which is connected to the
		 * data ready pin of the QMC5883L. Having this pin connected is mandatory
		 * for working properly.
		 *
		 */
		QMC5883L(int drdyPin);

		// Definition of types for the 'begin()' function defined below…
		/*!
		 * \enum OutputDataRate
		 * An enum type for output data rate selection. To be used as a parameter
		 * of the begin() function.
		 * The selected value will define at which period the QMC5883L magnetometer will
		 * issue magnetic field measurement (10 Hz => every 100 ms, 50 Hz => every 20 ms,
		 * 100 Hz => every 10 ms and 200 Hz => every 5 ms).
		 */
		enum OutputDataRate {ODR_10Hz = 0, /*!< 0 */
			ODR_50Hz, /*!< 1 */
			ODR_100Hz, /*!< 2 */
			ODR_200Hz  /*!< 3 */};

		/*!
		 * \enum FullScale
		 * An enum for resolution range selection. To be used as a parameter
		 * of the begin() function.
		 */
		enum FullScale {FS_2G = 0 /*!< 0 */, FS_8G /*!< 1 */};

		/*!
		 * \fn void begin()
		 *
		 * \brief Initialization function. Shall be called once at startup.
		 *
		 * \param
		 * rate 	the output data rate at which the QMC5883L will output the measured
		 * magnetic field values. The default is 10 Hz and other available values are 50 Hz,
		 * 100 Hz and 200 Hz (see the OutputDataRate enum).
		 * \param
		 * resolution 	the resolution or range of the measured magnetic field. The default
		 * value is 2G which means that the min and max values measured by the ADC correspond
		 * to a magnetic field ranging from -2G to +2G. Otherwise, a ±8G resolution [-8G … +8G]
		 * can be selected but in that case, the resolution is much lower than ±2G.
		 *
		 */
		void begin(OutputDataRate rate = ODR_10Hz, FullScale resolution = FS_2G);

		/*!
		 * \fn getDataReady()
		 *
		 * \brief Synchronise with magnetometer data ready.
		 *
		 * That function shall be used to know if the magnetometer has some measured
		 * data ready to be sent to the Arduino. It relies on the "drdyPin".
		 * This means that the 'DrdyPin' (output pin) of magnetometer shall be connected
		 * to an input pin of the Arduino.
		 *
		 * \return Return true if data are available for reading and in such a case,
		 * a read operation can be issued. Otherwise the function returns false.
		 *
		 * \warning It is highly recommended to check for data availability before actually
		 * reading data. This will provide a good synchronisation with the magnetometer
		 * and also avoid useless traffic on the I2C bus.
		 *
		 * Here is below a code snippet on how to use such function.
		 * \code
		 *
		 * // Test if the magnetometer has data ready for reading (check the DrdyPin)
		 * if(getDataReady() == true)
		 * {
		 *     // Data ready, read it!
		 *     Vector3f data = readData();
	 	 * }
		 * else
		 * {
		 *     // no data ready for reading, do something else
		 * }
		 *
		 * \endcode
		 */
		boolean getDataReady();

		/*!
		 * \fn setCalibrationData()
		 *
		 * \brief Set the magnetometer calibration data.
		 *
		 * By default, the magnetometer output values are not perfect. Most of the time,
		 * the (x, y, z) values represent a sphere not always centered on the (0, 0, 0) point
		 * and also not fully circular (flatten sphere).
		 * Using that function, it is possible to provide parameters that the QMC5883L class
		 * will apply to recenter the sphere on the center point and to compensate the flat
		 * part on the (X, Y) axis.
		 *
		 * \param
		 * xofs	the calibration offset on the X axis (in mGaus)
		 * \param
		 * yofs	the calibration offset on the Y axis (in mGaus)
		 * \param
		 * zofs	the calibration offset on the Z axis (in mGauss)
		 * \param
		 * xyratio	the y/x ratio to compensate the (X, Y) ellipsis (no unit, it is a ratio)
		 *
		 * \note By default, the calibration data are set to 0 meaning that no
		 * calibration is applied.
		 * Calibration data are computed by another mean and their storage is up to
		 * the application. The setCalibrationData() function is only the entry point
		 * by which it is possible to tell the QMC5883L class what compensation to apply.
		 *
		 */
		void setCalibrationData(float xofs, float yofs, float zofs, float xyratio);

		// Read current temperature (Experimental)
		/*!
		 * \fn getTemperature()
		 *
		 * \brief Return the internal magnetometer temperature.
		 *
		 * \return Returns the temperature in degrees Celsius
		 *
		*/
		float getTemperature();

		/*!
		 * \fn flushData()
		 *
		 * \brief Flush the available magnetometer data.
		 *
		 * That function actually issue a data read on the I2C bus but without
		 * returning them to the application. Such data are thrown away.
		 *
		 */
		void flushData();

		/*!
		 * \fn readRawData()
		 *
		 * \brief Read the magnetometer raw data.
		 *
		 * That function issues a read cycle on the I2C bus to get the magnetometer
		 * data back to the application.
		 * The data are 'raw' data meaning that they are not converted into mGauss values.
		 * It is directly the values output by the digital to analog converter.
		 *
		 * \return
		 * Returns a Vector3f containing the raw values on the 3 axis.
		 *
		 */
		Vector3f readRawData();

		/*!
		 * \fn readData()
		 *
		 * \brief Read the data converted to mGauss.
		 *
		 * That functions reads the data provided by the magnetometer and converts them
		 * to mGauss (milli-gauss) values based on the selected resolution (2G or 8G).
		 * If filtering is activated, a low pass filter is applied.
		 *
		 * \return
		 * Returns a Vector3f which contains the mGauss magnetic field on the 3 axis.
		 *
		 */
		Vector3f readData();

		/*!
		 * \fn setBaudRate()
		 *
		 * \brief Change the data refreshing of the QMC5883L.
		 *
		 * That function can be used to change the period at which the QMC5883L device
		 * will refresh the measured magnetic field data.
		 * The default value is 10 Hz.
		 *
		 * \param
		 * rate	a OutputDataRate enum that defines the available refresh periods.
		 *
		 */
		void setBaudRate(OutputDataRate rate);

		/*!
		 * \fn getHeading()
		 *
		 * \brief Compute the absolute heading.
		 *
		 * That function computes the absolute heading based on the true magnetic field values
		 * returned by the QMC5883L magnetometer on the 3 axis. Note that while the 3 axis
		 * values are used, the heading is on the horizontal plane only (X, Y).
		 *
		 * If filtering is activated, then the heading will be filtered using a low pass filter.
		 *
		 * If pitch and roll angles are provided using the setTiltAngles() function (coming
		 * from an accelerometer for example), then the heading is tilted compensated. That operation
		 * consists in rotating the heading by pitch and roll angles arount the Y and X axis using
		 * quaternions.
		 *
		 * \return
		 * Returns a float value ranging from 0.0 to 360.0 in degrees.
		 *
		 */
		float getHeading();

		// Enable (or disable) data filtering (low pass filter), default is disabled
		/*!
		 * \fn enableFiltering()
		 *
		 * \brief Enables or disables the low pass filter.
		 *
		 * By default, no low pass filter is applied. By using that function, it is possible
		 * to activate such filter to smooth measured values over the time in order to give
		 * more stability.
		 *
		 * \param
		 * status	set to true to enable filtering, or set to false to disable it
		 *
		 */
		void enableFiltering(bool status);

		/*!
		 * \fn setTiltAngles()
		 *
		 * \brief Set the tilt angles (for tilt compensation)
		 *
		 * To be able to compensate the heading, the QMC5883L class needs to have the
		 * pitch and roll angles periodically provided. Such angles can be measured by
		 * an accelerometer for example giving the gravity vector and allowing to compute
		 * the angles between such vector and the horizontal plane.
		 *
		 * \param
		 * pitch	the pitch angle in degrees around the Y axis (right axis)
		 *
		 * \param
		 * roll		the roll angle in degrees around the X axis (forward axis)
		 *
		 */
		void setTiltAngles(float pitch, float roll);

	private:
		// Pin on which data ready signal is connected
		int drdyPin_;

		// Resolution
		enum FullScale resolution_;

		float xofs_;	// Calibration offset for X axis
		float yofs_;	// Calibration offset for Y axis
		float zofs_;	// Calibration offset for Z axis
		float xyratio_;	// X/Y axis scaling ratio

		float pitch_;	// pitch angle (for tilt compensation)
		float roll_;	// roll angle (for tilt compensation)

		bool overflow;	// Set to 'true' in case of data overflow

		bool filtering;	// true if filtering shall be activated

		// An array to store history data
		// Array<QMC5883L_Data, QMC5883L_HISTORY_DATA> hData;
		Array<Vector3f, QMC5883L_HISTORY_DATA> hData;
		// Cumulative data
		Vector3f hTotal;
		// Index to loop through the history data
		uint8_t hIndex;

		// Write a 8 bits value into a register
		void writeRegister(uint8_t reg, uint8_t value);

		// Read a 8 bits register
		uint8_t readRegister(uint8_t reg);


};


#endif
