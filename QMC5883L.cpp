
#include <Wire.h>
#include <QMC5883L.h>
#include <Vector3f.h>
#include <Quaternion.h>

/*
   ____  __  __  _____ _____  ___   ___ ____  _             _               
  / __ \|  \/  |/ ____| ____|/ _ \ / _ \___ \| |           | |              
 | |  | | \  / | |    | |__ | (_) | (_) |__) | |        ___| | __ _ ___ ___ 
 | |  | | |\/| | |    |___ \ > _ < > _ <|__ <| |       / __| |/ _` / __/ __|
 | |__| | |  | | |____ ___) | (_) | (_) |__) | |____  | (__| | (_| \__ \__ \
  \___\_\_|  |_|\_____|____/ \___/ \___/____/|______|  \___|_|\__,_|___/___/
                                                                            
*/                                                                          

QMC5883L::QMC5883L(int drdyPin)
{
	drdyPin_ = drdyPin;
	xofs_ = 0.0;
	yofs_ = 0.0;
	zofs_ = 0.0;
	xyratio_ = 1.0;
	hIndex = 0;
	filtering = false;
	overflow = false;
	pitch_ = 0.0;
	roll_ = 0.0;
}

void QMC5883L::begin(OutputDataRate rate, FullScale resolution)
{
	byte b;
	
	// Set the data ready pin to INPUT mode
	pinMode(drdyPin_, INPUT);
	
	writeRegister(QMC5883L::QMC5883L_PERIOD_REG, 1);
	
	// Memorize the resolution
	resolution_ = resolution;
	
	// Create the configuration with resolution and refresh rate
	b = (0 << 6) | (resolution << 4) | (rate << 2) | 1;
	
	// Write configuration
	writeRegister(QMC5883L::QMC5883L_CONTROL_1_REG, b);
	writeRegister(QMC5883L::QMC5883L_CONTROL_2_REG, 0);
	
}

boolean QMC5883L::getDataReady()
{
	return(digitalRead(drdyPin_));
}


void QMC5883L::setCalibrationData(float xofs, float yofs, float zofs, float xyratio)
{
	xofs_ = xofs;
	yofs_ = yofs;
	zofs_ = zofs;
	xyratio_ = xyratio;
}


void QMC5883L::writeRegister(uint8_t reg, uint8_t value)
{
	Wire.beginTransmission(QMC5883L::QMC5883L_I2C_ADDR);
	Wire.write(reg);
	Wire.write(value);
	Wire.endTransmission();
}


uint8_t QMC5883L::readRegister(uint8_t reg)
{
	byte b;
	long st;
	
	Wire.beginTransmission(QMC5883L_I2C_ADDR);
	Wire.write(reg);
	Wire.endTransmission();
	
	Wire.requestFrom((int)QMC5883L_I2C_ADDR, 1);
	st = millis();
	// Wait for data being available with a 10 ms timeout
	while(Wire.available() == 0 && (millis() - st) < 10);
	b = Wire.read();
	
	return(b);
}

float QMC5883L::getTemperature()
{
	long st;
	float temp = -8000.0;
	
	Wire.beginTransmission(QMC5883L_I2C_ADDR);
	Wire.write(QMC5883L_TEMPERATURE_LSB_REG);
	Wire.endTransmission();
	
	Wire.requestFrom((int)QMC5883L_I2C_ADDR, 2);
	
	st = millis();
	while(Wire.available() < 2 && (millis() - st) < 10);
	if(Wire.available() == 2)
	{
		temp = (float)( (int)Wire.read() | ((int)Wire.read() << 8) ) / 100.0;
	}
	return(temp);
}

void QMC5883L::flushData()
{
	uint16_t b;
	
	Wire.beginTransmission(QMC5883L_I2C_ADDR);
	Wire.write(0);
	Wire.endTransmission();
	
	Wire.requestFrom((int)QMC5883L_I2C_ADDR, 6);
	b = Wire.read() | Wire.read() << 8;
	b = Wire.read() | Wire.read() << 8;
	b = Wire.read() | Wire.read() << 8;
}

Vector3f QMC5883L::readRawData()
{
	Vector3f rawValues;
	
	Wire.beginTransmission(QMC5883L_I2C_ADDR);
	Wire.write(QMC5883L_OUTPUT_DATA_X_LSB_REG);
	Wire.endTransmission();
	
	Wire.requestFrom((int)QMC5883L_I2C_ADDR, 7);
	rawValues.x_ = (float)(Wire.read() | Wire.read() << 8); 
	rawValues.y_ = (float)(Wire.read() | Wire.read() << 8);
	rawValues.z_ = (float)(Wire.read() | Wire.read() << 8);
	
	overflow = (Wire.read() & 0x2) >> 1;
	
	return(rawValues);
}

Vector3f QMC5883L::readData()
{
	Vector3f d = readRawData();
	
	switch(resolution_)
	{
		case FS_2G:
			d.x_ = d.x_ * 83e-6 - xofs_;
			d.y_ = (d.y_ * 83e-6 - yofs_) * xyratio_;
			d.z_ = d.z_ * 83e-6 - zofs_;
			break;
		case FS_8G:
			d.x_ = d.x_ * 333e-6 - xofs_;
			d.y_ = (d.y_ * 333e-6 - yofs_) * xyratio_;
			d.z_ = d.z_ * 333e-6 - zofs_;
			break;
	};
	
	if(filtering)
	{
		hTotal = hTotal + (d - hData[hIndex]);
		hData[hIndex++] = d;
		hIndex %= QMC5883L_HISTORY_DATA;
		return(hTotal / QMC5883L_HISTORY_DATA);
	}
	
	return(d);
}

void QMC5883L::setBaudRate(OutputDataRate rate)
{
	uint8_t b;
	
	b = readRegister(QMC5883L_CONTROL_1_REG);
	b &= ~0x0c; // Clear ODR bits (bits 2 & 3)
	b |= (rate << 2); // Set ODR bits with rate
	writeRegister(QMC5883L_CONTROL_1_REG, b);
}

/* Return the computed heading with tilt compensation, if roll and pitch angles are updated, otherwise
 * the rotated heading will be the same than the original heading.
 */
float QMC5883L::getHeading()
{
	// Read the data converted to mGauss
	Vector3f d = readData();
	
	// Build a rotation quaternion of roll angle around X axis (forward).
	Quaternion qXaxis = Quaternion(roll_, Vector3f::FORWARD);
	// Build a rotation quaternion of pitch angle around Y axis (right).
	Quaternion qYaxis = Quaternion(pitch_, Vector3f::RIGHT);
	
	// Compensate the pitch angle around the Y axis. The magnetometer data are
	// rotated by pitch angle around the Y axis.
	Vector3f r = qYaxis.Rotate(d);
	// Compensate the heading by rotating it by the roll angle around the X axis
	r = qXaxis.Rotate(r);
	// Now 'r' represent the magnetometer values on all axis with tilt compensation.
	
	// Calculate the heading and convert it in degrees.
	float heading = atan2(r.y_, r.x_) * 180.0 / M_PI;
	// Heading is translated to [0° ; 360°].
	if(heading < 0)
		heading += 360.0;

	// Return the computed heading value.
	return(heading);
}

void QMC5883L::enableFiltering(bool status)
{
	filtering = status;
}

void QMC5883L::setTiltAngles(float pitch, float roll)
{
	pitch_ = pitch;
	roll_ = roll;
}

